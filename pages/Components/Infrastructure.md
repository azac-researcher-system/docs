# Infrastructure 

The purpose of this project is to accommodate the parts involved in the infrastructure setup.

 - Scripts
   - Deployment
   - Monitoring
   - System Management
   - API hooks
 - IaC
   - Terraform
 - Usable command
 - Yaml files for Orchestrations
   - Kubernetes
   - Docker Swarm Mode

