# Monolitic Bot System

the program is web crawler for some sources. The program can search with key words and scraping web content.

## Using
 - RestAPI
 - Library

## Dependencies

### Language

 - Python 3.9

### Library
 - Python
   - Selenium
   - requests
   - BeautifulSoup4
   - pika

### Tools
 - bash
 - curl

### OS types
 - Linux OS

### Ports
 - 17080

## Code Architecture

![Monolitic Bot System](../../pics/monoliticbs.png)
