# API

I used Symfony framework for API and frontend.

## Dependencies

 - postgresql
 - PHP 8.1 

### Docker images

 - php:8.1.3-fpm-buster
 - nginx:1.21-alpine

### Used PHP Extention

#### PHP  Extention

 - pdo
 - pdo_pgsql
 - zip
 - xsl
 - gd
 - intl
 - opcache
 - exif
 - mbstring
 - bcmath

#### PHP Extension Community Library

 - xdebug
 - amqp

### Tools

 - composer
 - pecl
 - curl

## How to install

[the infrastructure](infrastructure.md) project files can install the project for docker swarm mode. you can start docker swarm mode and configure ufw. 

```bash
# build the dockerfile
infra-yaml/docker/infra_symfony/create-image/nginx/Dockerfile # image name api_nginx
infra-yaml/docker/infra_symfony/create-image/php/Dockerfile # image name api_php
# create services 
infra-yaml/docker/infra_symfony/stack.yaml

# or just run install.sh
infra-yaml/docker/infra_symfony/install.sh

# clone API and edit test.sh server variable. server variable is your server you should access ssh server name. 
# you should install rsync for client and server if you use localhost or cloned api files your server just install sync or change rsync command the files

# and import database for the project run api/data/database.txt at postgresql

# configure database connection variable api/.env file
 

```

