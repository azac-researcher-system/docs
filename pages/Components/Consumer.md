# Consumer

the project can pull the rabbitmq queue and postgresql database table. 

## Dependencies

### Language

 - Python 3.9

### Library

 - pika
 - psycopg2

### Tools

 - Bash
 - pip
