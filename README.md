# AZAC Researcher System Documents

The project can found a source or news and showing. The project can find a news source and show it briefly and also gives a link to the post. Thanks to its algorithm that makes it easy to research, it can find the most useful information on the Internet. 
The project can find a news source and show it briefly and also gives a link to the post. Thanks to its algorithm that makes it easy to research, it can find the most useful information on the Internet. makes it easy to find resources

## All Contents

### Documents

 - Components
   - [API](./pages/Components/API.md)
   - [Consumer](./pages/Components/Consumer.md)
   - [Monolithic Bot System](./pages/Components/MonolithicBS.md)
   - [Infrastructure](./pages/Components/Infrastructure.md)
   - [Other Services](./pages/Components/OtherServices.md)
 - Check Lists for infra
   - [Database Errors](./pages/CheckLists/DatabaseError.md)
   - [Troubleshoots for Service](./pages/CheckLists/TroubleshootsService.md)
 - ChangeLogs
   - [API](./pages/ChangeLogs/API.md)
   - [Consumer](./pages/ChangeLogs/Consumer.md)
   - [Monolithic Bot System](./pages/ChangeLogs/MonolithicBS.md)
   - [Infrastructure](./pages/ChangeLogs/Infrastructure.md)
 - [Install Infrastructure](./ChangeLogs/installInfra.md)

### Project Url

 - [API](https://gitlab.com/azac-researcher-system/api)
 - [Consumer](https://gitlab.com/azac-researcher-system/consumer)
 - [Monolitic Bot System](https://gitlab.com/azac-researcher-system/monolithic-bot-system)
 - [infrastructure](https://gitlab.com/azac-researcher-system/infrastructure)
 
## Component Description

 - Frontend + API : symfony framework, shows as posts the bot results
 - PostgreSQL : Main database for user, search keys and scraping data
 - Consumer : the program can read rabbitmq queue and write postgresql
 - RabbitMQ : get bots data and write queue and wait consumer, message broker
 - Monolitic Bot Systems : the program is web crawler for some sources. The program can search with key words and scraping web content 

### The System Desing 

![System Desing](./pics/softwarearchitec.png)

## Project Plan

I started this project based on my own problems and have developed it completely on my own, so I had to improve my knowledge of web programming at the beginning level. The work became easier when I determined the algorithm of the project and decided what I really wanted to do.

In fact, the biggest challenge in this project was the design of the monolithic bot system service. This service has a monolithic structure with many bots inside, but these bots could be divided into separate microservices. However, the structure is designed as a monolithic project according to the requirements of the future structure. This service was designed in this way to meet the high scalability needs and to ensure that information transfer between services is at the parameter level and almost real-time.

This is the biggest challenge I have faced in this project. And of course, because this is a final project for a practicum, it was necessary for the project to be completed in a very short period of time. Learning a framework and library that I was unfamiliar with and inadequate in their usage was among the challenges I faced during this process


